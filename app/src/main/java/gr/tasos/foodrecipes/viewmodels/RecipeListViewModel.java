package gr.tasos.foodrecipes.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import gr.tasos.foodrecipes.models.Recipe;
import gr.tasos.foodrecipes.repositories.RecipeRepository;

public class RecipeListViewModel extends ViewModel {

    private RecipeRepository mRecipeRepository;
    private boolean mIsViewingRecipies;
    private boolean isPerformingQuery;

    public RecipeListViewModel() {
        super();
        mRecipeRepository = RecipeRepository.getInstance();
        isPerformingQuery = false;
    }

    public LiveData<List<Recipe>> getRecipes() {
        return mRecipeRepository.getRecipes();
    }

    public LiveData<Boolean> isQueryExhausted() {
        return  mRecipeRepository.isQueryExhausted();
    }

    public void searchRecipesApi(String query, int pageNumber) {
        mIsViewingRecipies = true;
        isPerformingQuery = true;
        mRecipeRepository.searchRecipesApi(query, pageNumber);
    }

    public void searchNextPage() {
        if(!isPerformingQuery && mIsViewingRecipies && !isQueryExhausted().getValue()) {
            mRecipeRepository.searchNextPage();
        }
    }

    public boolean isViewingRecipies() {
        return mIsViewingRecipies;
    }

    public void setIsViewingRecipies(boolean mIsViewingRecipies) {
        this.mIsViewingRecipies = mIsViewingRecipies;
    }

    public void setIsPerformingQuery(boolean value) {
        isPerformingQuery = value;
    }

    public boolean isPerformingQuery() {
        return isPerformingQuery;
    }

    public boolean onBackPressed() {
        if(isPerformingQuery) {
            mRecipeRepository.cancelRequest();
            isPerformingQuery = false;
        }

        if(mIsViewingRecipies){
            mIsViewingRecipies = false;
            return false;
        }
        return true;
    }
}
