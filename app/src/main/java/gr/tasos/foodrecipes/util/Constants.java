package gr.tasos.foodrecipes.util;

public class Constants {

    public static final String BASE_URL = "https://www.food2fork.com";

    public static final String API_KEY = "5a52182451ebfc09d6be4187eb05b4a3";

    public static final int NETWORK_TIMEOUT = 3000;

    public static final String[] DEFAULT_SEARCH_CATEGORIES =
            {"Barbeque", "Breakfast", "Chicken", "Beef", "Brunch", "Dinner", "Wine", "Italian"};

    public static final String[] DEFAULT_SEARCH_CATEGORY_IMAGES =
            {
                    "barbeque",
                    "breakfast",
                    "chicken",
                    "beef",
                    "brunch",
                    "dinner",
                    "wine",
                    "italian"
            };
}
